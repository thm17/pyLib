from json import loads
from os import path
from sys import stderr


class Struct:
    """this is designed to mimic a c struct. i.e. an object that holds a series of key value pairs"""

    def __init__(self, **entries):
        self.__dict__.update(entries)


def dict_to_obj(elem):
    """convert a dictionary to a struct (any dictionaries in the dictionary will be converted as well

    :param elem: the dictionary to convert
    :return: a :class:`Struct` of the elements in the dictionary where any dictionaries are also converted to structs
    """
    if type(elem) is not dict:
        return elem
    res = {}
    for key, value in elem.items():
        res.update({
            key: dict_to_obj(value)
        })
    return Struct(**res)


def anonymous_class_object(names):
    """generate a struct of elements named according to the list of names where each value is set to none

    :param names: a list of the names to use for the elements in the struct
    :return: a struct cosisting of elements with the names specified in names
    """
    res = {}
    for name in names:
        res[name] = None
    return dict_to_obj(res)


def json_to_python(path):
    """read a json file and convert it to a python data structure

    :param path: the path of the json file to read
    :return: a python dictionary of the elements in the json file
    """
    try:
        with open(path, 'r') as json_file:
            json_string = json_file.read()
            return loads(json_string)
    except IOError as e:
        if e.errno == 2:
            print("File not found (" + path + ")", file=stderr)
        else:
            raise e


_CONFIG = json_to_python(path.dirname(path.realpath(__file__)) + "/config_management.config.json")


def load_config_dict(file_path, direct=False):
    """read a json config file as a dictionary

    :param file_path: the file path to use either the python file to load the config for or the path to the config file
    :param direct: if this is true, load the path directly, if not work out the config file from the path
    :return: a dictionary represeting the json file
    """
    if direct:
        return json_to_python(file_path)
    # todo alter this to consider init.py and use os package
    (filename, _, _) = file_path.rpartition('.')
    return json_to_python(filename + _CONFIG['config_extension'])


def json_to_python_object(path):
    """read a json file into a :class:`tom_lib.Struct` see :func:`file_management.json_to_python`

    :param path:the path of the json file to use
    :return: a :class:`tom_lib.Struct` representing the contents of the json file
    """
    return dict_to_obj(json_to_python(path))


def load_config(file_path, direct=False):
    """read a json config file into a :class:`tom_lib.Struct` see :func:`file_management.load_config_dict`

    :param file_path: the file path to use either the python file to load the config for or the path to the config file
    :param direct: if this is true, load the path directly, if not work out the config file from the path
    :return: a :class:`tom_lib.Struct` represeting the json file
    """
    return dict_to_obj(load_config_dict(file_path, direct))
