"""This is a library of functions that relates to file reading an writing and also loading configs
NB. this file is formatted specifically to avoid an import loop with tom_lib.py be careful when reformatting this file. The imported method is dict_to_obj
"""
from functools import partial
from operator import itemgetter
from os import getcwd
from os.path import join, split
from subprocess import PIPE, Popen, STDOUT

from .config_management import load_config
from ..tom_lib.general import search_groups

CONFIG = load_config(__file__)


def read_whole_file(path):
    """read the entire contents of a file into a string

    :param path: the path of the file to be read
    :return: the contents of the file as a string
    """
    with open(path, 'r') as file:
        return file.read()


def write_whole_file_fake(path: str, string: str, do_print: bool = True):
    print_string = ": \n".join([path, string])
    if do_print:
        print(print_string)
        return
    return print_string


def write_whole_file(path, string):
    """(over)write the contents of a file with a string

    :param path: the path to the file to be written
    :param string: the string to write
    :return: None
    """
    with open(path, 'w+') as file:
        file.write(string)


def path_swap(orig, *others):
    """Replace the tail of a path

    :param orig: the original path
    :param others: the remainder of the path
    :return:
    """
    return join(split(orig)[0], *others)


def generate_dependancies(path: str = None) -> None:
    """An imperfect dependancies generator for python

    :param path:
    :return:None
    """
    write_whole_file(
            join((getcwd() if path is None else path), CONFIG.requirements_file_name),
            "\n".join(
                    set(
                            map(
                                    itemgetter(0),
                                    filter(
                                            None,
                                            map(
                                                    partial(search_groups,
                                                            r".*:(?:from|import) ([^.]*?)[. ].*")
                                                    ,
                                                    Popen([
                                                        CONFIG.binaries.grep,
                                                        "-r",
                                                        "--include", "*.py",
                                                        "import",
                                                        "./"
                                                    ],
                                                            stdout=PIPE,
                                                            stderr=STDOUT,
                                                            cwd=path,
                                                            universal_newlines=True
                                                    ).communicate()[0].split("\n")
                                            )
                                    )
                            )
                    )
            )
    )