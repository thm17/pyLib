"""This is a general library of functions
NB. this file is formatted specifically to avoid an import loop with file_management.py be careful when reformatting this file. The imported method is dict_to_obj
"""
from pprint import PrettyPrinter
from random import randint
from re import search
from time import sleep
from typing import Generator
from warnings import warn

from ..file_management import load_config
from ..file_management.config_management import anonymous_class_object

CONFIG = load_config(__file__)
YES_NO = CONFIG.yes_no

PP = PrettyPrinter(indent=4)
ALPHABET = list("abcdefghijklmnopqrstuvwxyzF")


def copy_reset_struct(old_struct):
    """take a :class:tom_lib.Struct: and return a copy of new :class:tom_lib.Struct: with blanked values

    :param old_struct: the original struct
    :return: a new struct with the same keys as the old but all the values set to null
    """
    return anonymous_class_object(old_struct.__dict__.keys())


def none_coalesce(val, rep):
    """substitute of the null/None coalescing operator in php and c#
        if val is None, return rep, otherwise return val
    http://php.net/manual/en/migration70.new-features.php
    https://msdn.microsoft.com/en-gb/library/ms173224.aspx

    :param val: the value to check if it is None
    :param rep: the value to replace it with if it is None
    :return: rep if val is None, val otherwise
    """
    return rep if val is None else val


def none_coalesce_dict(search, key, rep=None):
    """Do Not Use, use dictionary.get() method instead
        https://www.tutorialspoint.com/python/dictionary_get.htm
    :param search: the dictionary to search
    :param key: the key to search for
    :param rep: the value to return if key is not found in search
    :return: search[key] if key is in search, rep otherwise
    """
    warn(
            "this should not be used, use dictionary.get() method instead\nhttps://www.tutorialspoint.com/python/dictionary_get.htm",
            DeprecationWarning)
    return search.get(key, rep)


def sleepm(milliseconds):
    """sleep for a number of milliseconds

    :param milliseconds: the milliseconds to sleep for
    :return: void
    """
    sleep(milliseconds / 1000.0)


def sleepu(microseconds):
    """sleep for a number of microseconds

    :param microseconds: the number of microseconds to sleep for
    :return: void
    """
    sleep(microseconds / 1000000.0)


def is_int(number_string):
    try:
        int(number_string)
    except ValueError:
        return False
    else:
        return True


def search_groups(pattern, string, flags=0):
    #todo consider walrus opperator from 3.8
    search_res = search(pattern, string, flags)
    if search_res is None:
        return []
    return search_res.groups()


def ask_yes_no(message, required=False, default_yes=True):
    options = ("y/N")
    if default_yes:
        options = ("Y/n")
    if required:
        options = ("y/n")
    while True:
        response = input(message + " " + options.join(["(", ")"])).lower().replace(" ", "")
        res = None
        if response in YES_NO.yes_list:
            res = True
        if response in YES_NO.no_list:
            res = False
        if res == None:
            if required:
                print("Please answer yes or no")
                continue
            res = default_yes
        return res


def rep_rand(min: int, max: int) -> Generator[int, None, None]:
    """Generator that yields random ints between min and max forever

    :param min: the minimum value (inclusive)
    :param max: the maximum value (inclusive)
    :return: a random int
    """
    while True:
        yield randint(min, max)
