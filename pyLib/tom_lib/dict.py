from functools import partial
from operator import itemgetter
from typing import List, Union, Iterable, Any, Dict, TypeVar, Callable, NewType
from warnings import warn

from pyLib.tom_lib.generics import T, U
from .functional import expand_generators, funclpar, compose, ppur


def dict_add_ret_kv(d, k, v):
    """set the element identified by the key k in the dictionary d to the value v and return a tuple of k and v

    :param d: the dictionary to add the element to
    :param k: the key of the element to be added/updated
    :param v: the value to be added
    :return: a tuple of k, v
    """
    d[k] = v
    return (k, v)


def dict_add_ret_d(d, k, v):
    """set the element identified by the key k in the dictionary d to the value v and return the dictionary d after the update

    :param d: the dictionary to add the element to
    :param k: the key of the element to be added/updated
    :param v: the value to be added
    :return: the dictionary d
    """
    d[k] = v
    return d


def dict_pop_ret_d(d, k):
    """pop the value in dictionary d identified by key k and return a tuple of the resulting value and the dictionary after the pop

    :param d: the dictionary to pop the element from
    :param k: the key of the element to pop (remove)
    :return: a tuple of the removed value and the dictionary after the removal
    """
    v = d.pop(k)
    return v, d


def multi_level_dict_get(dictionary: dict, keys: List[str], default="use_empty_dict"):
    """perform get on multiple levels of keys, returning the default value if the key does not exist
    if dictionary.get("key",default_value) equates to dictionary["key"] (if the value is there). This function
    does the equivalent for dictionary["key1"]["key2"]["key3"]
    :param dictionary: the dictionary to take the key from
    :param keys: a list of keys to search
    :param default: the default value to return (defaults to empty dictionary)
    :return: the value at the position in dictionary indicated by keys, or default if the keys do not exist
    """
    if default == "use_empty_dict":
        default = {}
    if len(keys) == 0:
        return dictionary
    value = dictionary
    for key in keys[:-1]:
        value = value.get(key, {})
    return value.get(keys[-1], default)


def subset_dictionary(search_dict: dict, key: Union[str, List[str]]) -> dict:
    """Debugging function to return dictionary of the key(s) and value(s) from a dictionary

    :param search_dict: the dictionary to extract from
    :param key: either a string of the key or a list of the keys desired from the dictionary
    :return: a dictionary of the key(s) and appropriate value(s) from search_dict
    """
    if isinstance(key, list):
        return dict(zip(key, map(search_dict.get, key)))
    return {
        key: search_dict[key]
    }


def dict_combine(elems: Iterable[List], key_index: int = 0, key_func: callable = None, single: bool = True,
                 list_replace=True) -> dict:
    """Effectively group by,

    :param elems: The elements to group
    :param key_index: the index within the element to use as the key
    :param key_func: the function to call upon the key element to return the key value
    :param single: whether the remainder of the list should be treated as a single value
    :param list_replace: convert the elements to a list
    :return: a dictionary of grouped elements
    """
    warn("Use dict_combine_revised", DeprecationWarning)
    res = {}
    for x in elems:
        if not isinstance(x, list) and list_replace:
            x = list(x)
        key = x[key_index] if key_func is None else key_func(x[key_index])
        if key not in res:
            res[key] = []
        if single:
            res[key].append(x[not key_index])
        else:
            res[key].append((y for i, y in enumerate(x) if i != key_index))
    return res


ElemsType = NewType("ElemsType", Union[Iterable[U], Dict[T, U]])


def _dict_combine_list_fetch(l: List[T], k: int):
    return (y for i, y in enumerate(l) if i != k)


def _dict_combine_dict_fetch(d: Dict[T, U], k: T):
    return {kd: kv for kd, kv in d.items() if kd != k}


def dict_combine_revised(elems: ElemsType, key: Union[int, T, Callable[[ElemsType], T]]):
    """Group by but with an iterable of dictionaries

    :param elems:Iterable of elements to be grouped
    :param key: either a function that returns the key element to use or an int (list/tuple index) or an alternative value for a dictionary index
    :return: a dictionary of values grouped by the key element
    """
    res = {}
    elems_iter = iter(elems)
    elem = elems_iter.__next__()
    key_is_func = callable(key)
    elem_func = _dict_combine_dict_fetch if isinstance(elem, dict) else _dict_combine_list_fetch
    if key_is_func:
        for elem in elems_iter:
            k = key(elem)
            if k not in res:
                res[k] = []
            res[k].append(elem_func(elem, k))
    else:
        for elem in elems_iter:
            k = elem[key]
            if k not in res:
                res[k] = []
            res[k].append(elem_func(elem, key))
    return res


def dict_list_to_list(d, null_val=None):
    """Convert a dictionary with int keys into a list where the index in the list is the key

    :param d: the dictionary to input
    :return: a generator of the dictionary values where the index is the key in the dict
    """
    return (d.get(i, null_val) for i in range(max(d.keys()) + 1))

# def recursive_dict_combine(levels, flat_list,top_level=True):
#     """combine dictionary elements in a recursive manner starting with the left most element
#         flat_list = [
#         ('a','a','a',1,2,3),
#         ('a','a','b',2,3,4),
#         ('a','a','c',3,4,5),
#         ('a','b',1,4,5,6),
#         ('a','b',2,5,6,7),
#         ('b','a',3,6,7,8)
#         ]
#         levels = 3
#
#         result = {
#             a:{
#                 a:{
#                     a:[1,2,3]
#                     b:[2,3,4]
#                     c:[3
#                 },
#                 b:{
#                 1:4,
#                 2:5
#                 }
#             },
#             b:{
#                 a:{
#                     3:6
#                 }
#             }
#         }
#
#     :param levels: the number of elements to take as keys
#     :param flat_list: a flat list of tuples
#     :return: a nested dictionary of elements
#     """
#     if levels == 0:
#         return expand_generators(map(funclpar([None, itemgetter(0)]), flat_list))
#     return expand_generators(map(funclpar(
#             [
#                 None,
#                 compose(
#                         dict,
#                         partial(recursive_dict_combine, levels - 1),
#                         expand_generators,
#                         ppur("items"),
#                         partial(dict_combine, single=False)
#                 )
#             ]), flat_list))
