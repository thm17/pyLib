from collections import deque
from functools import partial
from itertools import chain, filterfalse, islice, repeat, tee, zip_longest
from random import randrange
from typing import Any, Callable, Generator, Iterable, List, Tuple, TypeVar, Union
from warnings import warn

# todo check to see if you can run this without numpy if you don't import the one function
from numpy import unique

from pyLib.tom_lib.generics import T, U
from ..file_management import load_config

CONFIG = load_config(__file__)


# todo add @see elements to docstrings
def iview_through_window(input_iterable, frame_size=1):
    """Take an iterable and return a list of tuples as an iterator (hence the iview) representing a window into the sequence where the window is of width frame_size
        i.e. uses a sliding window of width frame_size presenting the current window at each time step
        e.g. list of letters in the alphabet ['a', 'b', 'c',.....
        with a frame size of 2 results in [('a', 'b'), ('b', 'c'), ('c', 'd'),......
    :param input_iterable: the iterable to group into elements
    :param frame_size: the size of the frame/window to use
    :return: a iterator of tuples where each is of length frame_size and are ofset by one from each other
    """
    if frame_size <= 0:
        warn("frame size must be at least 1", RuntimeWarning)
        return None
    return zip(*[islice(elem, i, None) for i, elem in enumerate(tee(input_iterable, frame_size))])


def view_through_window_distinct(input_iterable, frame_size=1):
    """This is a wrapper of :func:`iview_through_window` but results in a iterator which retuns non overlaping tuples
        e.g. list of letters in the alphabet ['a', 'b', 'c',.....
        with a frame size of 2 results in [('a', 'b'), ('c', 'd'), ('e', 'f'),...
    :param input_iterable: the iterable to group into elements
    :param frame_size: the size of the frame/window to use
    :return: a iterator of tuples where each is of length frame_size and are ofset by frame_size from each other
    """
    warn(DeprecationWarning, "Deprecated, use chunk instead")
    return islice(iview_through_window(input_iterable, frame_size), step=frame_size)


def chunk(input_iterable, chunk_size, fill_value=None):
    """An alias for view_through_window_distinct
    taken from:
    https://stackoverflow.com/a/312644
    :param input_iterable: the iterable to be read
    :param chunk_size:
    :param fill_value:
    :return:
    """
    return zip_longest(*[iter(input_iterable)] * chunk_size, fillvalue=fill_value)


def chunk_uneven_tail(input_iterable: Iterable[T], chunk_size: int) -> Generator[List[T], None, None]:
    """Split an iterable into lists of length chunk_size with the last list being the length of the remaining elements

    :param input_iterable: an input iterable
    :param chunk_size: the length of each of the output lists
    :return: A series of lists, each of length chunk_size wintil the last one
    """
    input_it = iter(input_iterable)
    while True:
        buffer = []
        for _ in range(chunk_size):
            try:
                buffer.append(input_it.__next__())
            except StopIteration:
                yield buffer
                return
        yield buffer


def chunk_split(input_iterable, chunk_count):
    """Split list into chunk_count evenly sized chunks

    :param input_iterable: the iterable to be split
    :param chunk_size: the size of each chunk
    :return: the list split into separate chunks
    """
    return list(map(list, [iter(input_iterable)] * chunk_count))


def filter_not_none(iterable):
    """filter an iterable returning a list of elements that are not None

    :param iterable: the iterable to filter
    :return: a list of elements in the iterable whose elements are not None
    """
    return filter(lambda elem: elem is not None, iterable)


def zip_length_elem(elems, index, fill_value=None):
    """Zip a group of elements up to the length of the element specified by index

    :param elems: the iterable of elements to be zipped
    :param index: the element to be zipped up to the length of
    :param fill_value: the value to pad elements who are too short with
    :return: the zipped set of elements
    """
    in_iters = [chain(elem) if i == index else chain(elem, repeat(fill_value)) for i, elem in enumerate(elems)]
    try:
        while in_iters:
            yield tuple(map(next, in_iters))
    except StopIteration:
        pass


def flatten_gen(input):
    """Flatten a multi level list of values into a single level, returning a generator

    :param input: the iterable of values
    :return: a generator of a single level list
    """
    if isinstance(input, str):
        yield input
        return
    try:
        for x in input:
            yield from flatten_gen(x)
    except TypeError as e:
        if "not iterable" not in e.__str__():
            raise e
        yield input


def expand_generators(elem):
    """Recursively itterates a data structure to expand generators, keeps dictionaries, lists and tuples as they are

    :param elem: the element to be expanded
    :return: a nested structure where each of the generator/map elements are expanded
    """
    type_name = type(elem).__name__
    if type_name == "dict":
        return {x: expand_generators(y) for x, y in elem.items()}
    if type_name in CONFIG.generator_types:
        return [expand_generators(x) for x in elem]
    if type_name == "tuple":
        return tuple(expand_generators(x) for x in elem)
    return elem


def infini_tee(input_iter):
    """produce a neverending list of teed iterable

    :param input_iter: the iterable to tee
    :return: a list of teed iterable as a generator
    """
    res, cur = tee(input_iter)
    while True:
        yield res
        res, cur = tee(cur)


inf_t_f = lambda input_iter: infini_tee(input_iter).__next__


def filter_true_false_ind(func, input_iter):
    """
    Filter a list returning both the correct and inverse lists
    :param func: the filtering func
    :param input_iter: the iterable to filter
    :return: the output
    """
    # inv_func = lambda x: not func(x)
    # return tuple(map(map,[filter,filterfalse],zip(repeat(func),tee(input_iter))))

    # tuple(map(map, [lambda x: "  " + str(x) + "  ", domino_to_string], zip(*enumerate(display_strings["playable"]))))

    res = tuple(map(lambda x, y: x(*y), [filter, filterfalse], zip(repeat(func), tee(input_iter))))
    return res
    # return filter(func, input_iter), filter(inv_func, input_iter)


class PlaceholderValue:
    pass


def filter_true_false_comb(func, input_iter):
    # return tuple(map(partial(filter,lambda x:x!=PlaceholderValue),zip(*((y,PlaceholderValue) if func else (PlaceholderValue,y) for y in input_iter))))
    # res = tuple(map(partial(filter,lambda x:x!=PlaceholderValue),zip(*((y,PlaceholderValue) if func(y) else (PlaceholderValue,y) for y in input_iter))))
    res = tuple(
            map(partial(filter, lambda x: x is not None),
                zip(*((y, None) if func(y) else (None, y) for y in input_iter))))
    return res


def split_iter(iterable, index):
    """Split an iterable into two parts

    :param iterable: the iterable to be split
    :param index: the index to split on
    :return:
    """
    return tuple(islice(iterabl, index, *x) for x, iterabl in zip([[], [None]], tee(iterable)))


# convience function for split iter
pspli = pspl = lambda iter_split_index: partial(split_iter, index=iter_split_index)


def compose(f, *funcs):
    """Take any number of functions and compose them. See function composition
        compose(f,g)(x) -> f(g(x))
    :param f: the first function
    :param funcs: the remaining functions
    :return: a function that passes the result of the last function to the result of the previous up the list.
    """
    if funcs:
        return lambda *x: f(compose(*funcs)(*x))
    return f


def rcompose(*funcs):
    """same as compose but with a reversed list

    :param x: the list of functions
    :return:a function that passes the result of the first function to the result of the next in the list.
    """
    return compose(*reversed(funcs))


def tup_list_extract(input_iter, index):
    """Split a list of tuples to get one element

    :param input_iter:
    :param index:
    :return:
    """
    return (x[index] for x in input_iter)


def shuffled(elems):
    """generator to produce a randomly shuffled list of elements

    :param elems:
    :return:
    """
    elems2 = elems
    while True:
        try:
            yield elems2.pop(randrange(len(elems2)))
        except (IndexError, ValueError):
            raise StopIteration


def purify(method_name: str, o: object, *args):
    """Convert an in object method to a function that takes the method name and object and calls the method on that object

    :param method_name: the name of the method to call on the object
    :param o: the object to call the method on
    :param args: the args to pass to the method
    :return: the result of calling the method with the args provided
    """
    # return o.__getattribute__(method_name)()
    return o.__getattribute__(method_name)(*args)


# convience wrapper function of purify for use in map
ppur = partial(partial, purify)


def star(func):
    """Convert a function that takes a set of arguments into one that takes a list of arguments
        star(zip)(some_list) => zip(*some_list)
    :param func: the function to convert
    :return: a function that takes a list of arguments and applies them to func with the * opperator
    """
    return lambda args: func(*args)

def invstar(func):
    """Inverse of star
    Convert a function that takes a single argument into one that takes a list of arguments
        invstar(func)(arg1,arg2,arg3,...) => func([arg1,arg2,arg3,...)
    :param func: the function to convert
    :return: a function that takes a list of arguments and applies them to func with the * opperator
    """
    return lambda *args:func(args)

def starstar(func):
    """Convert a function that takes a dictionary of arguments into one that takes a list of keyword arguments
        star(fun)(some_list) => fun(**some_list)
    :param func: the function to convert
    :return: a function that takes a list of arguments and applies them to func with the ** opperator
    """
    return lambda args: func(**args)

def function_list(funcs: Iterable[Callable[[U], T]], elems: Iterable[U]) -> Iterable[T]:
    """Apply an iterable of callables to an iterable of elements

    :param funcs: the list of callables
    :param elems: a list of elements
    :return: result
    """
    return map(lambda a, b: b if a is None else a(b), funcs, elems)


# convience wrapper function of function list
def funclpar(*funcs):
    return partial(function_list, funcs)

def _inner(lst: Iterable[Union[int, float]], i: int, v: int) -> Generator[int, None, bool]:
    """Internal generator, take a iterable of values and optionally add a value to them

    :param lst: the input iterable
    :param i: if this index is reached, add v else yield the element
    :param v: the value to add
    :return: the list elements unchanged, except for the i'th element, to which v is added.
    """
    for j, x in enumerate(lst):
        if i == j:
            # yield " + ".join(map(str,[x, v]))
            yield x + v
            continue
        yield x
    if i == j:
        return True
    return False


def variance(vals: Iterable[Union[int, float]], variation: int, single: bool = False) -> Generator[
    Union[int, List[int]], None, None]:
    """Generator that returns sets of each value adding and subtracting up to variance to one value at a time

    :param vals: the set of values to add
    :param variation: the ammount of variation
    :param single: if true, yield a single element else yeild a tuple
    :return:
    """
    for v in chain.from_iterable(range(i, (variation + 1) * i, i) for i in [-1, 1]):
        for i, l in enumerate(infini_tee(vals)):
            if single:
                if (yield from _inner(l, i, v)):
                    break
            else:
                res = list(_inner(l, i, v))
                if len(res) == i:
                    break
                yield res


def chain_varied(*vals: Iterable[T]) -> Generator[T, None, None]:
    """Chain both iterable and non iterable elements together

    :param vals: the values to iterate over
    :return: a list of the top level elements
    """
    for val in vals:
        if isinstance(val, str):
            yield val
        else:
            try:
                yield from val
            except TypeError:
                yield val


def val_counts(vals):
    """Wrapper for numpy unique return counts, true

    :param vals: the single level list to count the unique occurrences from
    :return: the set of values to return
    """
    return zip(*map(ppur("tolist"), unique(vals, return_counts=True)))


# convinence function for star chain varied
chain_s_varied = star(chain_varied)


def join_elems(separator: str) -> List[str]:
    """Like zip but join a series of lists with a separator
    
    :param separator: the separator to join the elements with
    :return: the list of strings
    """
    return compose(partial(map, partial(map, separator.join), zip))


def isli_par(*args) -> Callable[[Iterable], Iterable]:
    """PArtial wrapper for islice

    :return: function which takes and iterable and returns the result of islice applied with this iterable
    """
    return lambda x: islice(x, *args)


def bufferGenerator():
    q = deque()
    x = None
    while True:
        if x is not None:
            q.append(x)
            x = yield None
        else:
            try:
                x = yield q.popleft()
            except IndexError:
                return


def _ftf_gen(func, orig_gen, other_gen=None):
    # todo sort out how to keep track of other gen, will probs have to use class, see link for example
    # https://stackoverflow.com/questions/21808113/is-there-anything-similar-to-self-inside-a-python-generator
    """
    If other_gen is None
Other_gen = yield None
Else
Other_gen.send self

On Sat, 15 Sep 2018 17:07 Tom M, <metalmonkey.morrison@gmail.com> wrote:

    Will need to see if you can send to an expired generator

    On Sat, 15 Sep 2018 17:04 Tom M, <metalmonkey.morrison@gmail.com> wrote:

        This is really a buffering queue, could possibly be super-ed in another generator for filter true false

        On Sat, 15 Sep 2018 17:00 Tom M, <metalmonkey.morrison@gmail.com> wrote:

            If X not none
            Queue.push x
            X = yield none
            Else
            X = yield queue.pop
            #if empty queue.pop returns empty list

            On Sat, 15 Sep 2018 16:56 Tom M, <metalmonkey.morrison@gmail.com> wrote:

                If X is not none, queue.append X, yield none

                On Sat, 15 Sep 2018 16:54 Tom M, <metalmonkey.morrison@gmail.com> wrote:

                    Two generators that have the other generators send method (can probs be based on same generator) can hold a queue from send elements, if value is sent to gen, append to queue, else, yield pop queue else
    """
    if other_gen is None:
        other_gen = yield None
    else:
        other_gen.send(self)
    while True:
        next_elem = orig_gen.__next__()
        if func(orig_gen):
            yield next_elem
        else:
            other_gen.send(next_elem)


def filter_true_false(func, orig_gen):
    # todo write this and also
    tg = _ftf_gen(func, orig_gen)
    fg = _ftf_gen(compose(lambda x: not x, func), orig_gen, tg)
    return tg, fg


def isinst_par(typ: type) -> bool:
    """A partial application of the base function isinstance

    :param typ: the type to compare
    :return: a partially applied version of the isinstance function
    """
    return lambda x: isinstance(x, typ)


def slice_indices(list_len: Union[int, List[Any]], chunks) -> Iterable[Tuple[int]]:
    """Generate a list of the slice indicies for a list

    :param list_len: either a list or an int, if list_len is a list, the function will take the length
    :param chunks: the number of chunks to slice
    :return: an iterable of slice indicies for each list element
    """
    if isinstance(list_len, list):
        list_len = len(list_len)
    if not chunks:
        return [(None, None)]
    return iview_through_window(chain([None], range(chunks, list_len, list_len // chunks), [None]), 2)


def factors(n):
    divs = []
    for x in filter(lambda y: not n % y, range(1, n)):
        if x in divs:
            return
        div = n // x
        divs.append(div)
        yield x, div


# debug
if __name__ == '__main__':
    pass
