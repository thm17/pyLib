import sys
from atexit import register
from operator import itemgetter
from typing import Union, Optional, Iterable, List, Tuple, Generator, NewType, Iterator, Any

from pymysql import connect, OperationalError, Connection
from pymysql.cursors import Cursor

from ..file_management import load_config
from ..file_management.config_management import json_to_python, json_to_python_object

#
CONFIG = load_config(__file__)
CREDENTIALS = json_to_python(CONFIG.default_credentials_path)
CONNECTION: Connection = None
DbRes = NewType('DbRes', Union[List[Any], Tuple[List[Any], Iterator[str]]])


def set_credentials_path(path: str):
    """Change the credentials path search location for the data

    :param path:
    :return:
    """
    global CREDENTIALS
    crdnset = CREDENTIALS is None
    CREDENTIALS = json_to_python_object(path)
    if crdnset:
        # if there was no connection, connect to make one
        _reconnect()


def _reconnect():
    """create a database connection

    :return:
    """
    global CONNECTION
    try:
        CONNECTION = connect_with_credentials()
    except OperationalError:
        pass


def connect_with_credentials(name: str = None, socket_path: str = CONFIG.default_socket) -> Optional[Connection]:
    """Create a database connection with the specified credentials alias

    :param name: The credentials name to use
    :param socket_path: The path of the to use socket defaults to the socket file specified in this files config file
    :return: a connection to the database using the connections
    """
    global CREDENTIALS
    if CREDENTIALS is None:
        return None
    if name is None:
        try:
            name = CREDENTIALS.default_credentials
        except KeyError:
            sys.stderr.write("Default credentials key not found")
    try:
        creds = CREDENTIALS.credentials.__dict__[name]
    except KeyError:
        sys.stderr.write("credentials " + name + " not found")
        return None
    connection = connect(host=creds.host, user=creds.username, passwd=creds.password,
                         db=creds.database, unix_socket=socket_path)
    return connection


def insert_query(query: str, credentials_name: str = None):
    """Run an insert query on the database

    :param query: the query to run
    :return:
    """
    con = CONNECTION
    if credentials_name is not None:
        con = connect_with_credentials(credentials_name)
    cursor = con.cursor()
    cursor.execute(query)
    con.commit()
    cursor.close()


def run_insert_many(query: str, args: Iterable[Iterable[Any]], credentials_name: str = None):
    """Run a repeated insert/update query

    :param query: The query string
    :param args: The arguments as a list of tuples
    :return:
    """
    con = CONNECTION
    if credentials_name is not None:
        con = connect_with_credentials(credentials_name)
    cursor = con.cursor()
    cursor.executemany(query, args)
    cursor.close()


def run_multi_query(query: str, credentials_name: str = None, include_cols: bool = False) -> Generator[
    DbRes, Union[Iterable, None, bool], None]:
    """Execute the same query repeatedly with different arguments. The arguments are specified by using send

    :param query: The query to use
    :param credentials_name: the credentials to use on the query
    :param include_cols: if true, return the column names for the query
    :return: the query result and optionally the column names if include_cols is specified
    """
    cursor = _fetch_cursor(credentials_name)
    res = None
    while True:
        elems = yield res
        if elems is False:
            cursor.close()
            return
        cursor.execute(query, elems)
        query_res = cursor.fetchall()

        if include_cols:
            res = query_res, map(itemgetter(0), cursor.description)
        else:
            res = query_res


def run_query(query: str, elems: Iterable = None, credentials_name: str = None, include_cols: bool = False,
              close_cursor: bool = True) -> DbRes:
    """execute a query against the database, does not commit

    :param query: the query to execute
    :param elems: the parameters to be bound in the query
    :return: the query result as either a list of dictionaries or tuples
    """
    # todo consider setting cursor to just be opened for the query and then closed, will need alternate behaviour for repeated stored procedure (have param flag)
    cursor = _fetch_cursor(credentials_name)
    if elems is None:
        cursor.execute(query)
    else:
        cursor.execute(query, elems)
    res = list(cursor.fetchall())
    if include_cols:
        res = res, list(map(itemgetter(0), cursor.description))
    if close_cursor:
        cursor.close()
    return res


def _fetch_cursor(credentials_name: str) -> Cursor:
    """Fetch the cursor for the connection

    :param credentials_name: The name of the credentials to use
    :return: The cursor
    """
    if credentials_name is None:
        return CONNECTION.cursor()
    return connect_with_credentials(credentials_name).cursor()


def _destructor():
    """close the database connection"""
    print("Check to show mysql destructor has been called")
    if CONNECTION is not None:
        CONNECTION.close()


register(_destructor)
_reconnect()
