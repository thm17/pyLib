from atexit import register
from functools import partial
from itertools import chain, islice, tee
from typing import Callable, TypeVar, Iterable, List, Dict, Tuple
from .generics import T, R
from .functional import slice_indices
from multiprocess.pool import Pool

p = None


def get_pool() -> Pool:
    """Request the singleton instance of p (the pool)

    :return: the pool object
    """
    global p
    if p is None:
        p = Pool()
        register(p.terminate)
        return p
    return p


def para_map(func: Callable[[T], R], input_iter: Iterable[T]) -> List[R]:
    """run map in parallel

    :param func: the function to use for the map
    :param input_iter: the iterable to iterate over
    :return: the map result
    """
    return get_pool().map(func, input_iter)


def para_imap(func: Callable[[T], R], input_iter: Iterable[T]) -> Iterable[R]:
    """run imap in parallel

    :param func: the function to use for the map
    :param input_iter: the iterable to iterate over
    :return: the map result as an iterator
    """
    return get_pool().imap(func, input_iter)


KR = TypeVar("KR")


def para_dict_comp(func: Callable[[T], Tuple[KR, R]], input_iter: Iterable[T]) -> Dict[KR, R]:
    """parallel version of a dictionary comprehension

    :param input_iter: an iterable that is passed to the map function
    :param func: a function that returns a two element tuple that consist of the  keys and values for the resultant dictionary
    :return: a dictionary computed in parallel
    """
    return dict(get_pool().imap_unordered(func, input_iter))


def para_filter(filter_func: Callable[[T], bool], input_list: List[T]) -> Iterable[T]:
    """Return an iterator yielding those items of input_list for which filter_fun(item) is true. If function is None, return the items that are true.

    :param filter_func: this function is applied to every item in input_list, if it returns true, the item is appended to the result. If it is None, any items that evaluate to true are appended
    :param input_list: the input list to be iterated over
    :return: an iterable of all items in input_list which cause filter_func to return True. If filter_func is None, all items that evaluate to true
    """
    pool = get_pool()
    processes = pool._processes
    return chain.from_iterable(
            pool.imap(
                    partial(filter, filter_func),
                    map(
                            islice,
                            tee(input_list, processes),
                            *zip(*slice_indices(input_list, processes))
                    )
            )
    )


def para_filter_unordered(filter_fun: Callable[[T], bool], input_list: List[T]) -> Iterable[T]:
    """Parallel filter function unordered

    :param filter_fun:
    :param input_list:
    :return:
    """
    pool = get_pool()

    processes = pool._processes
    return chain.from_iterable(
            pool.imap_unordered(
                    partial(filter, filter_fun),
                    map(
                            islice,
                            tee(input_list, processes),
                            *zip(*slice_indices(input_list, processes))
                    )
            )
    )


if __name__ == '__main__':
    pass
