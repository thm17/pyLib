from functools import partial
from re import search, sub
from traceback import format_stack
from warnings import warn

from inflect import engine

from ..file_management import load_config
from .general import none_coalesce
from typing import List

pluralise = engine()
CONFIG = load_config(__file__)


def _var_replace_curry(var_vals):
    """this function subs variables in a string with their respective values

    :param var_vals: a dictionary of values to use for replacement
    :return: :func:`tom_lib._var_replace`
    """
    """this is a curried function (https://wiki.haskell.org/Currying)
    currying is essentially just a way of having multi argument functions when functions can only take one argument.
    It is the process of creating a function that takes one argument and returns a function that takes one argument.
    """

    def _var_replace(match):
        """ this function takes a regex match and returns its value in the vars dictionary or the match itself if it
        was not found

        :param match: the matched element in the search string
        :return: the value that should replace this match or the match string itself if it was not found in the vars
        dictionary
        """
        if match.group(1) in var_vals:
            return var_vals[match.group(1)]
        else:
            warn("var " + match.group(1) + " not in list\n" + ''.join(format_stack()), RuntimeWarning)
            return match.group(0)

    return _var_replace


def variable_sub(string, var_vals, pattern=None):
    """substitute variables indicated by @var_name@ in a string with the value at the index of var_name in the
    var_vals dictionary

    :param string: the string with variable names in
    :param var_vals: a dictionary of variable names and their values
    :param pattern: the pattern to use to search for the variable
    :return: the formatted string
    """
    pattern = none_coalesce(pattern, CONFIG.variable_pattern)
    try:
        return sub(pattern, _var_replace_curry(var_vals), string)
    except TypeError as e:
        # if the exception is raised for a reason other than that we passed a list to sub instead of a string, re-raise it
        if "expected string or Unicode, list found" not in str(e):
            raise
        return var_vals[search(pattern, string).group(1)]


def cased_var_sub(sub_string, case_vars):
    if isinstance(sub_string, list):
        return [cased_var_sub(string, case_vars) if isinstance(string, list) else variable_sub(string,
                                                                                               construct_cased_vars(
                                                                                                       case_vars)) for
                string in sub_string]
    return variable_sub(sub_string, construct_cased_vars(case_vars))


def construct_cased_vars(case_vars):
    variable_cases = CONFIG.variable_cases.__dict__
    res = {}
    for var_name, cases in case_vars.items():
        for case_name, case_item in cases.items():
            res[variable_cases[case_name] + var_name] = case_item
    return res


def make_case_string(words, unchange=False):
    """make a dictionary of strings in the following cases:
        * CammelUpper
        * cammelLower
        * spaces between words
        * snake_case (underscores)

    :param words: the phrase to convert as a list of the words in it
    :return: a :class:`Cases` class
    """
    if unchange:
        return {
            'camel_up':    None,
            'camel_low':   None,
            'space':       None,
            'under_score': None,
            "unchanged":   words
        }
    cup = ''.join([ident.title() for ident in words])
    clow = words[0].lower() + ''.join([ident.title() for ident in words[1:]])
    space = ' '.join([ident.lower() for ident in words])
    score = '_'.join([ident.lower() for ident in words])
    return {
        'camel_up':    cup,
        'camel_low':   clow,
        'space':       space,
        'under_score': score,
        "unchanged":   None
    }


def case_split_name(str, split_char="_"):
    return make_case_string(str.split(split_char), False)


def list_to_indented_string(lst, prechars=''):
    return '\n'.join(
            list_to_indented_string(x, prechars + CONFIG.indent_character) if isinstance(x, list) else prechars + x for
            x in lst)


def lower(string):
    """convenience function to take a string and return the lowercase version. Allows application on a map function

    :param string: the string to lower
    :return: the result of string.lower()
    """
    return string.lower()


def splt(string: str, sep=None) -> List[str]:
    return string.split(sep)


psplt = partial(splt)
