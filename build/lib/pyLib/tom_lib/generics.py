"""Typing module generics

"""
from typing import TypeVar

T = TypeVar("T")
R = TypeVar("R")
U = TypeVar("U")
