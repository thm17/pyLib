import collections
from functools import partial
from typing import Generator, List, Iterable, Union, TypeVar, Callable, Any, Tuple

from pyLib.tom_lib.generics import T, U


def sub_list(minuend, subtrahend):
    """ returns a list of all the elements in lista and not in listb

    :param minuend: the list to subtract from
    :param subtrahend: the list to subtract
    :return: a list of all the elements in lista and not in listb
    """
    return [elem for elem in minuend if elem not in subtrahend]


def chunk_list(input_list, chunk_size):
    """chunk the list input list into a tuple of lists of size chunk_size

    :param input_list: the list to be split into chunks
    :param chunk_size: the size of each chunk
    :return: a tuple of lists containing the elements in the first list where each inner list is of size chunk_size
    """
    return tuple(input_list[pos:pos + chunk_size] for pos in range(0, len(input_list), chunk_size))


def flatten(input_itterator):
    """take a itterator (list or similar) of values and flatten it into a single level list
    take from http://stackoverflow.com/questions/2158395/flatten-an-irregular-list-of-lists-in-python

    :param input_itterator:an itterator consisting of multiple levels
    :return: an itterator of a flattened version of the list
    """
    for el in input_itterator:
        if isinstance(el, collections.Iterable) and not isinstance(el, str):
            for sub in flatten(el):
                yield sub
        else:
            yield el


def list_get(lst, index, alt=None):
    """replicating functionality of dict.get method but for lists
    if the index is valid, return lst[index]. Else, return alt

    :param lst: the list to search in
    :param index: the index to fetch from the list
    :param alt: the value to return if index is not in lst (defaults to None
    :return: the value at index in lst or alt if index is not in lst
    """
    try:
        return lst[index]
    except IndexError as e:
        return alt


def multi_level_list_get(lst: List[T], indexes: List[int], alt: U = "use_empty_list") -> Union[T, U]:
    """fetch value from multi dimensional list based on variable list of key elements. If a key is not found, return alt
    lst = [
        [],
        [
            [],
            [],
            ["hi"]
        ]
    ]
    multi_level_list_get(lst,[1,2]) => "hi"

    :param lst: the list to search through for each index
    :param indexes: the list of indexes to select from each level of the list
    :param alt: the value to return if any of the indexes are not in lst at their respective level (defaults to empty list)
    :return: the value at index in lst or alt if index is not in lst
    """
    if alt == "use_empty_list":
        alt = []
    for index in indexes:
        try:
            lst = lst[index]
        except IndexError:
            return alt
    return lst


def build_list(fn, start, n_times=0):
    """Build a list from a single element
    taken from https://stackoverflow.com/a/33745827
    :param fn: the function to build the list with
    :param start: the initial element to use
    :param n_times: number of times to produce 0 means run forever
    :return:
    """
    if n_times == 0:
        while True:
            start = fn(start)
            yield start
    for _ in range(n_times):
        start = fn(start)
        yield start


def split_list(list, index):
    """Split a list at index and return a tuple of the two parts start to index and from index (inclusive to end

    :param list: the list to be split
    :param index: the index at which to split the list
    :return: a two element tuple of list[:index] and list[index:]
    """
    return list[:index], list[index:]


# convience function for split list
pspl = lambda list_split_index: partial(split_list, index=list_split_index)


def pop_ret_list(lst, index):
    """return a copy of the list lst with the element at index removed

    :param lst: the list to remove the element from
    :param index: the index of the element to remove
    :return: a copy of lst without the element at index
    """
    return lst[:index] + lst[index + 1:]


def tup_elem_in_list(search, lst, index):
    """return true if search is at the specified index in the list of tuples
    i.e. treat the list of tuples as a table where each tuple is a row and search though the specified column (0 indexed) for "search"
    e.g. with the following paramters: search = 3,lst = [(1,2,3),(4,5,6),(7,8,9)],index = 1
        it will look through the 1st index of each tuple finding 2,5 and 8.
        It will try to see if each of these in turn are equal to 3 and will return false if not
    :param search: the element to search for
    :param lst: the list of tuples to search in
    :param index: the tuple index to search through
    :return: true if the search is found, false otherwise
    """
    return any(search == elem[index] for elem in lst)


def tup_index_in_list(search, lst, index):
    """mimics the functionality of list.index but with a sublist
    it will search for the first occurrence of search at the position index in each element in lst
    e.g. with search="a",lst = [(1,2,"b"),(False,6,"a"),(8,"stuff","c")],index = 2
    it will look through the list at each element and see if the item at the second index of each element matches search
    in this example, it will return 1
    :param search: the element to search for
    :param lst:the list of elements to search in
    :param index: the position at each element in the list to compare to search to check for a match
    :return: the position of the element in lst where element[index] matches search or None if there is no match
    """
    for i, elem in enumerate(lst):
        if elem[index] == search:
            return i
    return None


def chunk_list_stopper(input_iter: Iterable[T], delimiter: Union[Callable[[T], bool], T]) -> Generator[
    List[T], None, None]:
    """Take a list of elements and split it into sub lists where the list element matches delimiter (or where delimiter returns true)

    :param input_iter: This is the input iterable
    :param delimiter: this is either:
        - a function that takes each element in input iter and returns a boolean indicating whether to break or not.
        - a value that appears in input iter that input iter should be split on
    :return: a generator that produces the split sublist elements
    """
    buffer = []
    delimiter = delimiter if callable(delimiter) else lambda x: x == delimiter
    for elem in input_iter:
        buffer.append(elem)
        if delimiter(elem):
            yield buffer
            buffer = []
    if buffer:
        yield buffer
