import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
        name="toms_py_lib_package",
        version="0.0.1",
        author="Tom Morrison",
        author_email="thm17@aber.ac.uk",
        description="A collection of helper functions and other components",
        url="https://github.com/pypa/example-project",
        packages=setuptools.find_packages(),
        package_data={
            '': ['LICENCE','*.config.json'],
        },
        include_package_data=True,
        classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
        ],
)
